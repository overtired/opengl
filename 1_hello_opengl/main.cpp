#include <GL/glut.h>

/**
 * The animated rectangle
 */

GLfloat spin = 0.0f;

void init() {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_FLAT);
}


void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    glRotatef(spin, 0.0f, 0.0f, 1.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glRectf(-25.0f, -25.0f, 25.0f, 25.0f);
    glPopMatrix();
    glutSwapBuffers();
}

void spinDisplay() {
    spin += 1.0;
    if (spin > 360.0) {
        spin -= 360;
    }
    glutPostRedisplay();
}

void reshape(int width, int height) {
    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void mouse(int button, int state, int x, int y) {
    switch (button){
        case GLUT_LEFT_BUTTON:
            glutIdleFunc(spinDisplay);
            break;
        case GLUT_RIGHT_BUTTON:
            glutIdleFunc(NULL);
            break;
    }
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(250, 250);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Animated rectangle");
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutMainLoop();

    return 0;
}
