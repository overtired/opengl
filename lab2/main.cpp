#include <iostream>
#include <cmath>
#include <GL/glut.h>

using namespace std;

double X(double t, double t2, double x1, double x2)
{
    return x1 + t + (3.0 * (x2 - x1) / (t2 * t2) - 2.0 / t2 - 1.0 / t2) * t * t + (2.0 * (x1 - x2) /
                                                                                   (t2 * t2 * t2) + 1.0 / (t2 * t2) + 1.0 / (t2 * t2)) * t * t * t;
}

double Y(double t, double t2, double y1, double y2)
{
    return y1 + t + (3.0 * (y2 - y1) / (t2 * t2) - 2.0 / t2 - 1.0 / t2) * t * t + (2.0 * (y1 - y2) /
                                                                                   (t2 * t2 * t2) + 1.0 / (t2 * t2) + 1.0 / (t2 * t2)) * t * t * t;
}

double T2(double x1, double x2, double y1, double y2)
{
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(3.0);
    glColor3f(1, 1, 0);
    glBegin(GL_POINTS);

    double x1 = 152.0;
    double x2 = 260.0;
    double y1 = 140.0;
    double y2 = 60.0;

    double t2 = T2(x1, x2, y1, y2);

    for (double t = 0.0; t < 134.4; t += 1) {
        cout << t << " " << X(t, t2, x1, x2) << " " << Y(t, t2, y1, y2) << endl;
        glVertex3f(X(t, t2, x1, x2) / 360, Y(t, t2, y1, y2) / 240, 0.0);
    }

    glEnd();

    glutSwapBuffers();
}

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example");
    glClearColor(0, 0, 0, 0.0);
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}
