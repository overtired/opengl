#include <GL/glut.h>

GLUquadricObj* theqwNine;
GLfloat lightPositionNine[] = {0, 0, 10, 0};
GLfloat ModelAmbient[] = {1.0, 1.0, 1.0, 1.0};
GLfloat whiteLight[] = {1.0, 0.0, 0.0, 1.0};

void initNine()
{
    glClearColor(0.0, 0.0, 0.0, 1);
    theqwNine = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glLightfv(GL_LIGHT0, GL_AMBIENT, whiteLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteLight);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ModelAmbient);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPositionNine);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-5, 5, -5, 5, 5, 15);
    glMatrixMode(GL_MODELVIEW);
}

void displayNine()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glTranslated(0, 0, -10);
    glRotatef(45.0, 1.0, 1.0, 1.0);
    glutSolidTeapot(2);
    glPopMatrix();
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeNine()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example eight");
    initNine();
    glutDisplayFunc(displayNine);
    glutMainLoop();
}
