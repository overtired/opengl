#include <GL/glut.h>

GLUnurbsObj* nurbsObjTwelve;
GLfloat ctlArrayTwelve[4][3] = {{-0.9f, -0.8f, 0.0f}, {-0.2f, 0.8f, 0.0f}, {0.2f, -0.5f, 0.0f}, {0.9f, 0.8f, 0.0f}};

void initTwelve()
{
    glClearColor(0, 0, 0, 0);
    nurbsObjTwelve = gluNewNurbsRenderer();
    gluNurbsProperty(nurbsObjTwelve, GLU_SAMPLING_TOLERANCE, 25.0);
}

void displayTwelve()
{
    // Node-vector
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(3.0);
    glColor3f(0, 0.3f, 1);

    // Generating NURB-line
    GLfloat nodes[] = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
    gluNurbsCurve(nurbsObjTwelve, 7, nodes, 3, &ctlArrayTwelve[0][0], 3, GL_MAP1_VERTEX_3);
    glPointSize(8.0);

    glBegin(GL_POINTS);
    glColor3f(0.0, 0.0, 1.0);

    for (int i = 0; i < 4; i++) {
        glVertex3f(ctlArrayTwelve[i][0], ctlArrayTwelve[i][1], ctlArrayTwelve[i][2]);
    }

    glEnd();

    //    glBegin(GL_LINE_STRIP);
    //    glColor4f(0.0, 1.0, 1.0, 0.9);
    //    for (int i = 0; i < 4; i++) {
    //        glVertex3f(ctlArrayTwelve[i][0], ctlArrayTwelve[i][1], ctlArrayTwelve[i][2]);
    //    }
    //    glEnd();

    glColor3f(1.0, 1.0, 0);
    GLfloat nodes2[] = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
    gluNurbsCurve(nurbsObjTwelve, 8, nodes2, 3, &ctlArrayTwelve[0][0], 4, GL_MAP1_VERTEX_3);

    glFlush();
}

void executeTwelve()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example twelve");
    initTwelve();
    glutDisplayFunc(displayTwelve);
    glutMainLoop();
}
