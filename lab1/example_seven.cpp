#include <GL/glut.h>

GLUquadricObj* theqwSeven;

void initSeven()
{
    glClearColor(0.1, 0.98, 0.3, 1);
    theqwSeven = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-5, 5, -5, 5, 15, 20);
    glMatrixMode(GL_MODELVIEW);
}

void displaySeven()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(0.7, 0.7, 0);
    glPushMatrix();
    glTranslated(0, 0, -16);
    glRotatef(0.0, 1.0, 1.0, 1.0);
    glutSolidTeapot(3);
    glPopMatrix();
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeSeven()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example seven");
    initSeven();
    glutDisplayFunc(displaySeven);
    glutMainLoop();
}
