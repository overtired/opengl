#include <GL/glut.h>

#include <stdio.h>

GLUquadricObj* theqwEleven;
GLenum mode;
GLint hits = 0;

void drawSphereEleven()
{
    GLfloat koort[][2] = {{100, 140}, {120, 120}, {140, 100}, {160, 80}};

    for (int i = 0; i < 4; i++) {
        if (i == 0) {
            glColor4f(1, 0, 0, 0.2);
        }

        if (i == 1) {
            glColor4f(0, 1, 1, 0.4);
        }

        if (i == 2) {
            glColor4f(0, 0, 1, 0.7);
        }

        if (i == 3) {
            glColor4f(0, 1, 0, 0.9);
        }

        if (mode == GL_SELECT) {
            glLoadName(i);
        }

        glPushMatrix();
        glTranslatef(koort[i][0], koort[i][1], -50);
        gluSphere(theqwEleven, 40, 50, 50);
        glPopMatrix();
    }
}

void initEleven()
{
    glClearColor(1.0, 1.0, 1.0, 1);
    GLfloat lightPosition[] = {0, 0, -100, 0};
    theqwEleven = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    mode = GL_RENDER;
}

void displayEleven()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDepthMask(GL_FALSE);
    drawSphereEleven();
    glDepthMask(GL_TRUE);
    glutPostRedisplay();
    glutSwapBuffers();
}

void pickEleven(int button, int state, int x, int y)
{
    GLint viewPort[4];
    GLuint selectBuf[16];

    if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN) {
        return;
    }

    glGetIntegerv(GL_VIEWPORT, viewPort);
    glSelectBuffer(16, selectBuf);

    mode = GL_SELECT;
    glRenderMode(mode);
    glInitNames();
    glPushName(0);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble)x, (GLdouble)(viewPort[3] - y), 4.0, 4.0, viewPort);
    glOrtho(0, 240, 0, 240, 10, 240);
    drawSphereEleven();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    hits = selectBuf[3];
    mode = GL_RENDER;
    printf("%d\n", hits);
    glRenderMode(mode);
    glutSwapBuffers();
    glutPostRedisplay();
}

void reshapeEleven(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 240, 240, 0, 10, 240);
    glMatrixMode(GL_MODELVIEW);
}

void executeEleven()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(240, 240);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example eleven");
    initEleven();
    glutMouseFunc(pickEleven);
    glutReshapeFunc(reshapeEleven);
    glutDisplayFunc(displayEleven);
    glutMainLoop();
}

