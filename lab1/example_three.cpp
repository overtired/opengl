#include <GL/glut.h>

void displayThree()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1, 1, 0);
    glPushMatrix();
    glTranslatef(0.001, 0, 0);
    glutWireSphere(0.6, 20, 20);
    glPopMatrix();
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeThree()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(480, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example three");
    glClearColor(0, 0, 0, 1);
    glutDisplayFunc(displayThree);
    glutMainLoop();
}
