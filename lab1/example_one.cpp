#include <GL/glut.h>

void displayOne()
{
    // Clear display
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0, 1, 1);

    // Line drawing.
    glLineWidth(8);
    glBegin(GL_LINES);
    glVertex2d(-0.5, 0);
    glVertex2d(0.5, 0.0);
    glEnd();

    // Updating window
    //    glutPostRedisplay(); // Marks the current window to be redisplayed.
    glutSwapBuffers(); // Swap some buffers.
}

void executeOne()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example one");
    glClearColor(0, 0, 0, 0);
    glutDisplayFunc(displayOne);
    glutMainLoop();
}




