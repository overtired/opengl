#include <GL/glut.h>

GLUnurbsObj* nurbsObj;
GLfloat ctlArray[4][3] = {{-0.9f, -0.8f, 0.0f}, {-0.2f, 0.8f, 0.0f}, {0.2f, -0.5f, 0.0f}, {0.9f, 0.8f, 0.0f}};


void initFour()
{
    glClearColor(0, 0, 0, 0);
    nurbsObj = gluNewNurbsRenderer();
    gluNurbsProperty(nurbsObj, GLU_SAMPLING_TOLERANCE, 25.0);
}

void displayFour()
{
    // Node-vector
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(3.0);
    glColor3f(0, 0.3f, 1);

    // Generating NURB-line
    GLfloat nodes[] = {0.0, 0.0, 0.0, 1.0, 2.0, 2.0, 2.0};
    gluNurbsCurve(nurbsObj, 7, nodes, 3, &ctlArray[0][0], 3, GL_MAP1_VERTEX_3);
    glPointSize(4.0);

    glBegin(GL_POINTS);
    glColor3f(0.0, 0.0, 1.0);

    for (int i = 0; i < 4; i++) {
        glVertex3f(ctlArray[i][0], ctlArray[i][1], ctlArray[i][2]);
    }

    glEnd();

    glBegin(GL_LINE_STRIP);
    glColor3f(0.0, 1.0, 1.0);

    for (int i = 0; i < 4; i++) {
        glVertex3f(ctlArray[i][0], ctlArray[i][1], ctlArray[i][2]);
    }

    glEnd();

    glColor3f(1.0, 1.0, 0);
    GLfloat nodes2[] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0};
    gluNurbsCurve(nurbsObj, 8, nodes2, 3, &ctlArray[0][0], 4, GL_MAP1_VERTEX_3);

    glFlush();
}

void executeFour()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example four");
    initFour();
    glutDisplayFunc(displayFour);
    glutMainLoop();
}
