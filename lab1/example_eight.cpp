#include <GL/glut.h>

GLUquadricObj* theqwEight;
GLfloat lightPositionEight[] = {0, 0, 10, 0};

void initEight()
{
    glClearColor(0.1, 0.98, 0.3, 1);
    theqwEight = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glLightfv(GL_LIGHT0, GL_POSITION, &lightPositionEight[0]);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-5, 5, -5, 5, 5, 15);
    glMatrixMode(GL_MODELVIEW);
}

void displayEight()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(1, 0, 0);
    glPushMatrix();
    glTranslated(0, 0, -10);
    glRotatef(45.0, 1.0, 1.0, 1.0);
    glutSolidCube(5);
    glPopMatrix();
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeEight()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example eight");
    initEight();
    glutDisplayFunc(displayEight);
    glutMainLoop();
}
