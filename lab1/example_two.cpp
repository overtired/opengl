#include <GL/glut.h>

bool left = true;
double split = 0.0;

void displayTwo()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1, 1, 0);

    if (left) {
        glTranslatef(0.005, 0, 0); // Moves axes
        split += 0.005;

        if (split > 0.2) {
            left = false;
        }
    } else {
        glTranslatef(-0.005, 0, 0);
        split -= 0.005;

        if (split < -0.2) {
            left = true;
        }
    }

    glutWireSphere(0.7, 10, 70);// Paramters defines the quantity of drawing
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeTwo()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(480, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example two");
    glClearColor(0, 0, 0, 0);
    glutDisplayFunc(displayTwo);
    glutMainLoop();
}
