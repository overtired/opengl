#include <GL/glut.h>

GLUquadricObj* theqwFive;
GLfloat diffuseFive[] = {1.0, 1.0, 1.0, 1.0};
GLfloat positionFive[] = {1.0, 1.0, -1.0, 0.0};

void initFive()
{
    glClearColor(0, 0, 0, 0);
    theqwFive = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseFive);
    glLightfv(GL_LIGHT0, GL_POSITION, positionFive);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    //glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);
}

void displayFive()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glRotatef(0.1, 1.0, 1.0, 1.0);
    glColor3f(1, 1, 0);
    glutSolidCube(1);
    gluSphere(theqwFive, 0.3, 50, 50);
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeFive()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(480, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example five");
    initFive();
    glutDisplayFunc(displayFive);
    glutMainLoop();
}
