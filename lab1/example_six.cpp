#include <GL/glut.h>

GLUquadricObj* theqwSix;
GLfloat light0PositionSix[] = {10, 10, -60, 0};
GLfloat light1PositionSix[] = {10, 1, -60, 10};

void initSix()
{
    glClearColor(0, 0, 0, 0);
    theqwSix = gluNewQuadric();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glLightModelfv(GL_LIGHT0 | GL_POSITION, light0PositionSix);
    glLightModelfv(GL_LIGHT1 | GL_POSITION, light1PositionSix);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT0, GL_POSITION, light0PositionSix);
    glLightfv(GL_LIGHT1, GL_POSITION, light1PositionSix);
}

void displaySix()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glRotatef(1.0, 0.0, 1.0, 0.0);
    glColor3f(0.5, 0.5, 0);
    glutSolidTeapot(0.3);
    glutPostRedisplay();
    glutSwapBuffers();
}

void executeSix()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(720, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Example six");
    initSix();
    glutDisplayFunc(displaySix);
    glutMainLoop();
}
