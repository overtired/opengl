TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    example_one.cpp \
    example_two.cpp \
    example_three.cpp \
    example_four.cpp \
    main.cpp \
    example_five.cpp \
    example_six.cpp \
    example_seven.cpp \
    example_eight.cpp \
    example_nine.cpp \
    example_ten.cpp \
    example_eleven.cpp \
    example_twelve.cpp

LIBS += -lglut -lGL -lGLU

HEADERS +=
